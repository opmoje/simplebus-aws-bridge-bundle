<?php

namespace BNNVARA\SimpleBusAwsBridgeBundle\Command;

use BNNVARA\SimpleBusAwsBridge\Queue\QueueName;
use BNNVARA\SimpleBusAwsBridge\Queue\QueueProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConsumeCommand extends Command
{
    /** @var QueueProcessor */
    private $processor;

    public function __construct(QueueProcessor $processor)
    {
        $this->processor = $processor;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('bnnvara:simplebus:consume')
            ->setDescription('Consume messages from a given AWS queue')
            ->addArgument('queue', InputArgument::REQUIRED, 'Name of the queue to consume');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->processor->consume(new QueueName($input->getArgument('queue')));

        return 0;
    }
}