<?php

namespace BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    private $alias;

    public function __construct(string $alias = 'simple_bus_aws_bridge_bundle')
    {
        $this->alias = $alias;
    }

    /** @inheritdoc */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder($this->alias);

        if (method_exists($treeBuilder, 'getRootNode')) {
            // Symfony 4.2 +
            $rootNode = $treeBuilder->getRootNode();
        } else {
            // Symfony 4.1 and below
            $rootNode = $treeBuilder->root($this->alias);
        }

        $rootNode
            //->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('credentials')->isRequired()
                    ->children()
                        ->scalarNode('key')->isRequired()->end()
                        ->scalarNode('secret')->isRequired()->end()
                        ->scalarNode('token')->defaultNull()->end()
                        ->scalarNode('expires')->defaultNull()->end()
                        ->scalarNode('region')->isRequired()->end()
                    ->end()
                ->end()
                ->arrayNode('topic')
                    ->children()
                        ->scalarNode('name')->isRequired()->info('The arn of the topic you want to connect to')->end()
                        ->arrayNode('credentials')
                            ->children()
                                ->scalarNode('key')->isRequired()->end()
                                ->scalarNode('secret')->isRequired()->end()
                                ->scalarNode('token')->defaultNull()->end()
                                ->scalarNode('expires')->defaultNull()->end()
                                ->scalarNode('region')->isRequired()->end()
                            ->end()
                        ->end()
                        ->scalarNode('version')->isRequired()->end()
                        ->variableNode('asynchronous_events')->defaultValue([])->end()
                    ->end()
                ->end()
                ->arrayNode('queue')
                    ->children()
                        ->scalarNode('url')->isRequired()->end()
                        ->arrayNode('credentials')
                            ->children()
                                ->scalarNode('key')->isRequired()->end()
                                ->scalarNode('secret')->isRequired()->end()
                                ->scalarNode('token')->defaultNull()->end()
                                ->scalarNode('expires')->defaultNull()->end()
                                ->scalarNode('region')->isRequired()->end()
                            ->end()
                        ->end()
                        ->scalarNode('version')->isRequired()->end()
                        ->scalarNode('envelope_converter')
                            ->defaultValue('BNNVARA\SimpleBusAwsBridge\Queue\Consumer\SqsToEnvelopeConverter')
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}